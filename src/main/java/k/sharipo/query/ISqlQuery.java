package k.sharipo.query;

import k.sharipo.communication.Query;

import java.sql.Connection;

public interface ISqlQuery {
    public Query doSqlQuery(Connection connection);
}
