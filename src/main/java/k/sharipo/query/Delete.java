package k.sharipo.query;

import k.sharipo.communication.Query;
import k.sharipo.proxy.ProxyFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class Delete implements ISqlQuery {

    final private Logger logger = LoggerFactory.getLogger(Delete.class);

    private Query query;

    public Delete(Query query) {
        this.query = query;
    }

    @Override
    public Query doSqlQuery(Connection connection) {
        query.getWordMap().clear();
        try {
            PreparedStatement statement = ProxyFactory.makePrepareStatement("delete from dictionary where (word = ?)", connection);
            statement.setString(1, query.getWord());
            statement.executeUpdate();
            query.getWordMap().put(query.getWord(), query.getDescription());
            logger.info("word: " + query.getWord() + " deleted");
            connection.close();
        } catch (SQLException e) {
            System.out.println(e.getSQLState());
            logger.error(e.getSQLState());
        }
        return query;
    }
}
