package k.sharipo.query;

import k.sharipo.communication.Query;
import k.sharipo.proxy.ProxyFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;

public class FindMask implements ISqlQuery {

    final private Logger logger = LoggerFactory.getLogger(FindMask.class);

    private Query query;

    public FindMask(Query query) {
        this.query = query;
    }

    @Override
    public Query doSqlQuery(Connection connection) {
        query.getWordMap().clear();
        try {
            PreparedStatement statement = ProxyFactory.makePrepareStatement("select * from dictionary where word like ?", connection);
            statement.setString(1, query.getWord());
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                query.getWordMap().put(resultSet.getString(2), resultSet.getString(3));
            }
            logger.info("word: " + query.getWord() + " found by mask");
            connection.close();
        } catch (SQLException e) {
            System.out.println(e.getSQLState());
            logger.error(e.getSQLState());
        }
        return query;
    }
}
