package k.sharipo.query;

import k.sharipo.communication.Query;
import k.sharipo.proxy.ProxyFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class Create implements ISqlQuery {

    final private Logger logger = LoggerFactory.getLogger(Create.class);

    private Query query;

    public Create(Query query) {
        this.query = query;
    }

    @Override
    public Query doSqlQuery(Connection connection) {
        query.getWordMap().clear();
        try {
            PreparedStatement statement = ProxyFactory.makePrepareStatement("insert into dictionary (word, description) values (?, ?)", connection);
            statement.setString(1, query.getWord());
            statement.setString(2, query.getDescription());
            statement.executeUpdate();
            query.getWordMap().put(query.getWord(), query.getDescription());
            logger.info("word: " + query.getWord() + " created");
            connection.close();
        } catch (SQLException e) {
            System.out.println(e.getSQLState());
            logger.error(e.getSQLState());
        }
        return query;
    }
}
