package k.sharipo.query;

import k.sharipo.communication.Query;
import k.sharipo.proxy.ProxyFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class Change implements ISqlQuery {

    final private Logger logger = LoggerFactory.getLogger(Change.class);

    private Query query;

    public Change(Query query) {
        this.query = query;
    }

    @Override
    public Query doSqlQuery(Connection connection) {
        PreparedStatement statement = null;
        try {
            statement = ProxyFactory.makePrepareStatement("update dictionary set description = (?) where word = (?)",
                    connection);
            statement.setString(1, query.getDescription());
            statement.setString(2, query.getWord());
            statement.executeUpdate();
            query.getWordMap().put(query.getWord(), query.getDescription());
            logger.info("word: " + query.getWord() + " changed");
            connection.close();
        } catch (SQLException e) {
            System.out.println(e.getSQLState());
            logger.error(e.getSQLState());
        }

        return query;
    }
}
