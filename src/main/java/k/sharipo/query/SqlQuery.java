package k.sharipo.query;

import com.google.gson.Gson;
import k.sharipo.communication.Query;
import k.sharipo.proxy.ProxyFactory;

import java.sql.Connection;

public class SqlQuery {

    private String typeQuery;

    final static Gson gson = new Gson();

    public Query doSql(Connection connection, String json) {

        Query query = gson.fromJson(json, Query.class);

        switch (typeQuery) {
            case "create": {
                Create create = new Create(query);
                return ProxyFactory.makeSqlQuery(connection, create);
            }
            case "find": {
                Find find = new Find(query);
                return ProxyFactory.makeSqlQuery(connection, find);
            }
            case "delete": {
                Delete delete = new Delete(query);
                return ProxyFactory.makeSqlQuery(connection, delete);
            }
            case "change": {
                Change change = new Change(query);
                return ProxyFactory.makeSqlQuery(connection, change);
            }
            case "findMask": {
                FindMask findmask = new FindMask(query);
                return ProxyFactory.makeSqlQuery(connection, findmask);
            }
        }
        return null;
    }
}
