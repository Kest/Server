package k.sharipo.query;

import k.sharipo.communication.Query;
import k.sharipo.proxy.ProxyFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.*;

public class Find implements ISqlQuery {

    final private Logger logger = LoggerFactory.getLogger(Find.class);

    private Query query;

    public Find(Query query) {
        this.query = query;
    }

    @Override
    public Query doSqlQuery(Connection connection) {
        query.getWordMap().clear();
        try {
            PreparedStatement statement = ProxyFactory.makePrepareStatement("select * from dictionary where (word = ?)", connection);
            statement.setString(1, query.getWord());
            ResultSet resultSet = statement.executeQuery();
            ResultSet proxyResultSet = ProxyFactory.makeStubResultSet("Введите слово", "Заглушка на \"Введите слово\"", resultSet);
            while (resultSet.next()) {
                query.getWordMap().put(proxyResultSet.getString(2), proxyResultSet.getString(3));
            }
            logger.info("word: " + query.getWord() + " found");
            connection.close();
        } catch (SQLException e) {
            System.out.println(e.getSQLState());
            logger.error(e.getSQLState());
        }
        return query;
    }
}
