package k.sharipo.proxy;

import k.sharipo.communication.Query;
import k.sharipo.query.Create;
import k.sharipo.query.ISqlQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalTime;

public class ProxyFactory {

    final private static Logger logger = LoggerFactory.getLogger(ProxyFactory.class);

    static String strMarker;

    public static Query makeSqlQuery(Connection connection, Object object){
        ISqlQuery iSqlQuery = (ISqlQuery) Proxy.newProxyInstance(Create.class.getClassLoader(), Create.class.getInterfaces(), (proxy, method, args) -> {

            StringBuilder sb = new StringBuilder( LocalTime.now().toString() )
                    .append(" ")
                    .append(object.getClass().getName() + "." +method.getName() + "()");
            logger.info( sb.toString() );

            return method.invoke(object, args);
        });
        return iSqlQuery.doSqlQuery(connection);
    }

    public static PreparedStatement makePrepareStatement(String sql, Connection conn) {
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            PreparedStatement psProxy = (PreparedStatement) Proxy.newProxyInstance(ClassLoader.getSystemClassLoader(), new Class<?>[]{PreparedStatement.class}, new InvocationHandler() {
                String[] strings = new String[sql.length() - sql.replace("?", "").length()];
                @Override
                public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

                    StringBuilder sb = new StringBuilder( LocalTime.now().toString() );
                    sb.append(" ");

                    String methodName = method.getName()+"()";
                    if (method.getName().startsWith("set")){
                        strings[Integer.parseInt(args[0].toString())-1] = args[1].toString();
                    }
                    if (method.getName().equals("executeUpdate")||method.getName().equals("executeQuery")) {
                        strMarker = strings[0];
                        methodName = method.getName()+change(strings, sql);
                    }

                    long start = System.nanoTime();
                    Object returnObj = method.invoke(ps, args);
                    long finish = System.nanoTime();

                    sb.append( (finish-start) )
                            .append("ns ")
                            .append( methodName );
                    logger.info( sb.toString() );

                    return returnObj;
                }
            });
            return psProxy;
        } catch (SQLException e) {
            logger.error(e.getSQLState());
        }

        return null;
    }

    public static ResultSet makeStubResultSet(String strQuery, String strStub, ResultSet resultSet){
        ResultSet proxyResultSet = (ResultSet) Proxy.newProxyInstance(ClassLoader.getSystemClassLoader(), new Class<?>[]{ResultSet.class}, (proxy, method, args) -> {
            if (method.getName().equals("getString")){
                if (args[0].toString().equals("3")){
                    if (strQuery.equals(strMarker)){
                        return stub(strStub);
                    }
                    return method.invoke(resultSet, args);
                }
            }
            return null;
        });
        return proxyResultSet;
    }

    private static String change(String[] strings, String str){
        int index;
        for (int i = 0; i < strings.length; i++) {
            index = str.indexOf("?");
            str = str.substring(0, index) + "\"" + strings[i] + "\"" + str.substring(index+1);
        }
        logger.info(str);
        return str;
    }

    private static String stub(String strStub){
        return strStub;
    }
}
