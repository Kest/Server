package k.sharipo.communication;

import java.util.HashMap;

public class Answer {
    private String answer;
    private int id;
    private String word;
    private String description;
    private String error;
    private HashMap<String, String> wordMap = new HashMap();

    public Answer() {
    }

    public String getAnswer() {
        return this.answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getWord() {
        return this.word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String decription) {
        this.description = decription;
    }

    public String getError() {
        return this.error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public HashMap<String, String> getWordMap() {
        return this.wordMap;
    }

    public void setWordMap(HashMap<String, String> wordMap) {
        this.wordMap = wordMap;
    }
}
