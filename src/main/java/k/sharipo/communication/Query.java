package k.sharipo.communication;

import java.util.HashMap;

public class Query {
    private String typeQuery;
    private int id;
    private String word;
    private String description;
    private String error = "";
    private HashMap<String, String> wordMap = new HashMap();
    String sqlQuery;

    public Query() {
    }

    public String getTypeQuery() {
        return this.typeQuery;
    }

    public void setTypeQuery(String typeQuery) {
        this.typeQuery = typeQuery;
    }

    public int getId() {
        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getWord() {
        return this.word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getError() {
        return this.error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public HashMap<String, String> getWordMap() {
        return this.wordMap;
    }

    public void setWordMap(HashMap<String, String> wordMap) {
        this.wordMap = wordMap;
    }
}