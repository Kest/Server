package k.sharipo;

import com.google.gson.Gson;
import k.sharipo.communication.Answer;
import k.sharipo.db.DBConnection;
import k.sharipo.communication.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import k.sharipo.query.SqlQuery;
import k.sharipo.socket.SocketConnection;

import java.io.*;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.sql.*;

public class Server {

    final private Logger logger = LoggerFactory.getLogger(Server.class);

    static private SocketConnection socketConnection;
    static private ServerSocket serverSocket;
    static private InputStream inputStream;
    static private OutputStream outputStream;

    static private ByteArrayOutputStream baos;

    static Gson gson = new Gson();

    static Server server = new Server();

    static String jsonQuery;
    static String jsonAnswer;
    static String dbPath = "src/main/resources/db.json";
    static String socketPath = "src/main/resources/socket.json";
    static String serverError = "";
    private static String dbError = "";

    static int inLength;


    String strAnswer;

    SqlQuery query;

    Query query1;

    Answer answer;

    static FileReader scReader = null;
    static FileReader dbReader = null;

    static DBConnection dbConnection = null;
    static byte[] out;
    static int i;

    public static void main(String[] args) {

        Logger logger = LoggerFactory.getLogger(Server.class);

        logger.info("Сервер запущен");

        try {
            scReader = new FileReader(new File(socketPath));
            socketConnection = gson.fromJson(scReader, SocketConnection.class);
            serverSocket = new ServerSocket(socketConnection.getPort(), socketConnection.getQuantity(), InetAddress.getByName(socketConnection.getName()));
        } catch (FileNotFoundException e) {
            logger.error(e.getMessage());
        } catch (RuntimeException e) {
            logger.error(e.getMessage());
        } catch (UnknownHostException e) {
            logger.error(e.getMessage());
        } catch (IOException e) {
            logger.error(e.getMessage());
        }

        try {
            dbReader = new FileReader(new File(dbPath));
            dbConnection = gson.fromJson(dbReader, DBConnection.class);
        } catch (FileNotFoundException e) {
            logger.error(e.getMessage());
        } catch (RuntimeException e) {
            logger.error(e.getMessage());
        }

        while (true) {

            serverError = "";

            baos = new ByteArrayOutputStream();
            try (Socket connection = serverSocket.accept();) {
                jsonQuery = "";
                inputStream = connection.getInputStream();
                outputStream = connection.getOutputStream();

                baos.write(inputStream.read());
                inLength = inputStream.available();

                for (int j = 0; j < inLength; j++) {
                    i = inputStream.read();
                    baos.write(i);
                }

                jsonQuery = baos.toString();

                baos.close();

                jsonAnswer = server.dbUpdate(jsonQuery, dbConnection);

                out = jsonAnswer.getBytes();
                outputStream.write(out);
            } catch (IOException e) {
                logger.error(e.getMessage());
            }catch (RuntimeException e){
                logger.error(e.getMessage());
            }
        }
    }

    private String dbUpdate(String str, DBConnection dbConnection) {

        try {
            Class.forName(dbConnection.getJdbc());
        } catch (ClassNotFoundException e) {
            logger.error(e.getMessage());
        } catch (RuntimeException e) {
            logger.error(e.getMessage());
        }

        try (Connection connection = DriverManager.getConnection(dbConnection.getURL(), dbConnection.getName(), dbConnection.getPassword())) {
            query = gson.fromJson(str, SqlQuery.class);
            query1 = query.doSql(connection, str);
            answer = gson.fromJson(gson.toJson(query1), Answer.class);
        } catch (SQLException e) {
            serverError = dbError + "Отсутствует соединение с БД ";
            logger.error(e.getMessage());
            answer = new Answer();
            answer.setError(serverError);
        } catch (RuntimeException e) {
            serverError = dbError + "k.sharipo.Server Error: " + e;
            logger.error(e.getMessage());
            answer = new Answer();
            answer.setError(serverError);
        } finally {
            try {
                dbReader = new FileReader(new File(dbPath));
                this.dbConnection = gson.fromJson(dbReader, DBConnection.class);
            } catch (FileNotFoundException e) {
                logger.error(e.getMessage());
                serverError = "На сервере отсутствует файл БД соединения";
            }
            strAnswer = gson.toJson(answer);
            return strAnswer;
        }
    }
}
